<?php

class CreateIssuesTest extends FeatureTestCase
{
    function test_a_user_create_an_issue()
    {
        // Having
        $title = 'Esta es una incidencia';
        $content = 'Esta es la descripción de la incidencia';
        $this->actingAs($user = $this->defaultUser());
        // When
        $this->visit(route('issues.create'))
            ->type($title, 'title')
            ->type($content, 'content')
            ->press('Guardar');
        // Then
        $this->seeInDatabase('issues', [
            'title' => $title,
            'content' => $content,
            'user_id' => $user->id,
            'slug' => 'esta-es-una-incidencia',
        ]);
        // Test a user is redirected to the posts details after creating it.
        $this->see($title);
    }

    function test_creating_a_post_requires_authentication()
    {
        $this->visit(route('issues.create'))
            ->seePageIs(route('login'));
    }

    function test_create_issue_form_validation()
    {
        $this->actingAs($this->defaultUser())
            ->visit(route('issues.create'))
            ->press('Guardar')
            ->seePageIs(route('issues.create'))
            ->seeErrors([
                "title"   => 'El campo título es obligatorio',
                "content" => 'El campo contenido es obligatorio'
            ]);
    }
}