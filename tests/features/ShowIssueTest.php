<?php
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShowIssueTest extends FeatureTestCase
{
    function test_a_user_can_see_the_issue_details()
    {
        // Having
        $user = $this->defaultUser([
            'name' => 'José Manuel',
        ]);
        $issue = $this->createIssue([
            'title' => 'Este es el titulo de la incidencia',
            'content' => 'Este es el contenido de la incidencia',
            'tags' => 'tag1 tag2 tag3',
            'user_id' => $user->id,
        ]);

        // When
        $this->visit(route('issues.show', $issue))
            ->seeInElement('h1', $issue->title)
            ->see($issue->content)
            ->see($user->name);
    }
}