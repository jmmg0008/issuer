<?php

use App\Issue;
use Carbon\Carbon;


class IssueListTest extends FeatureTestCase
{

    function test_a_user_can_see_the_issues_list_and_go_to_the_details()
    {
        $issue = $this->createIssue([
            'title' => '¿Debo usar Laravel 5.3 o 5.1 LTS?'
        ]);
        $this->visit('/')
            ->seeInElement('h1', 'Incidencias')
            ->see($issue->title)
            ->click($issue->title)
            ->seePageIs($issue->url);
    }

    function test_the_issues_are_paginated()
    {
        // Having...
        $first = factory(Issue::class)->create([
            'title' => 'Incidencia más antigua',
            'created_at' => Carbon::now()->subDays(2)
        ]);
        $posts = factory(Issue::class)->times(15)->create([
            'created_at' => Carbon::now()->subDay()
        ]);
        $last = factory(Issue::class)->create([
            'title' => 'Incidencia más reciente',
            'created_at' => Carbon::now()
        ]);
        $this->visit('/')
            ->see($last->title)
            ->dontSee($first->title)
            ->click('2')
            ->see($first->title)
            ->dontSee($last->title);
    }
}
