<?php


class ExampleTest extends FeatureTestCase
{
    
    function test_basic_example()
    {
        $user = factory(\App\User::class)->create([
                'name' => 'Jose Manuel',
                'email' => 'admin@jmmoreno.com',
            ]);

        $this->actingAs($user, 'api')
            ->get('api/user')
            ->see('Jose Manuel')
            ->see('admin@jmmoreno.com');
    }
}
