<?php
use Illuminate\Foundation\Testing\DatabaseTransactions;
class IssueIntegrationTest extends TestCase
{
    use DatabaseTransactions;

    function test_a_slug_is_generated_and_saved_to_the_database()
    {
        $issue = $this->createIssue([
            'title' => 'Como instalar Laravel',
        ]);

        $this->assertSame(
            'como-instalar-laravel',
            $issue->fresh()->slug
        );
        /*
                $this->seeInDatabase('issues', [
                    'slug' => 'como-instalar-laravel'
                ]);
                $this->assertSame('como-instalar-laravel', $issue->slug);
        */
    }
}