<?php
use App\Issue;
class PostModelTest extends TestCase
{
    function test_adding_a_title_generates_a_slug()
    {
        $issue = new Issue([
            'title' => 'Como instalar Laravel'
        ]);
        $this->assertSame('como-instalar-laravel', $issue->slug);
    }
    function test_editing_the_title_changes_the_slug()
    {
        $issue = new Issue([
            'title' => 'Como instalar Laravel'
        ]);
        $issue->title = 'Como instalar Laravel 5.1 LTS';
        $this->assertSame('como-instalar-laravel-51-lts', $issue->slug);
    }
}