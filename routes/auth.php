<?php
// Routes that require authentication.
// Posts
Route::get('issues/create', [
    'uses' => 'CreateIssueController@create',
    'as' => 'issues.create',
]);
Route::post('issues/create', [
    'uses' => 'CreateIssueController@store',
    'as' => 'issues.store',
]);