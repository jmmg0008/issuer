<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', [
        'uses' => 'IssueController@index',
        'as' => 'issues.index'
        ]);

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('issues/{issue}', [
    'as' => 'issues.show',
    'uses' => 'IssueController@show'
])->where('issue', '\d+');