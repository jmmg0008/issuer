@extends('layouts/app')

@section('content')
    <h1>{{ $issue->title }}</h1>

    <p>{{ $issue->content }}</p>

    <p>{{ $issue->tags }}</p>

    <p>{{ $issue->user->name }}</p>
@endsection