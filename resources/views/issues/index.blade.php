@extends('layouts.app')

@section('content')
    <h1>Incidencias</h1>

    <ul>
        @foreach($issues as $issue)
            <li>
                <a href="{{ $issue->url }}">
                    {{ $issue->title }}
                </a>
            </li>
        @endforeach
    </ul>

    {{ $issues->render() }}

@endsection