<?php

namespace App\Http\Controllers;
use App\Issue;
use Illuminate\Http\Request;

class IssueController extends Controller
{

    public function index()
    {
        $issues = Issue::orderBy('created_at', 'DESC')->paginate();;
        return view('issues.index', compact('issues'));
    }

    public function show(Issue $issue)
    {
        return view('issues.show', compact('issue'));
    }
}

