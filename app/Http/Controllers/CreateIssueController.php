<?php

namespace App\Http\Controllers;

use App\Issue;
use Illuminate\Http\Request;

class CreateIssueController extends Controller
{
    //

    function create(){

        return view('issues.create');
    }

    function store(Request $request){

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $issue = new Issue($request->all());
        auth()->user()->issues()->save($issue);
        return "Post: ".$issue->title;
    }
}
